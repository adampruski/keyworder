import re
from bs4 import BeautifulSoup
from bs4.element import Comment
from requests import get
from unicodedata import normalize


def get_html(url):
    r = get(url)
    r.raise_for_status()
    return r.text


def get_soup(html, parser="html.parser"):
    soup = BeautifulSoup(html, parser)
    return soup


def get_keywords(soup):
    metas = soup.findAll(attrs={"name": re.compile(r"keywords", re.I)})
    keywords = []
    for meta in metas:
        keywords_raw_list = meta["content"].split(",")
        for keyword_raw in keywords_raw_list:
            keyword = normalize_caseless(keyword_raw.strip())
            keywords.append(keyword)
    return keywords


def is_tag_visible(element):
    if element.parent.name in [
        "style",
        "script",
        "head",
        "title",
        "meta",
        "[document]",
    ]:
        return False
    if isinstance(element, Comment):
        return False
    return True


def get_text(soup):
    texts = soup.body.findAll(text=True)
    visible_texts = filter(is_tag_visible, texts)
    text = u" ".join(t.strip() for t in visible_texts)
    return normalize_caseless(text)


def count_keyword(text, keyword):
    return text.count(keyword)


def normalize_caseless(text):
    return normalize("NFKD", text.casefold())


def get_statistics(text, keywords):
    stats = {}
    for keyword in keywords:
        count = count_keyword(text, keyword)
        stats[keyword] = count
    return stats


def go(url, parser="html.parser"):
    html = get_html(url)
    soup = get_soup(html, parser)
    keywords = get_keywords(soup)
    text = get_text(soup)
    return get_statistics(text, keywords)
