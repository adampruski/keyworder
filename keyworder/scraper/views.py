from .forms import URLForm
from .scraper import go
from django.shortcuts import render
from django.views.generic.edit import FormView


class HomePageView(FormView):
    template_name = "home.html"
    form_class = URLForm

    def form_valid(self, form):
        url = form.cleaned_data["url"]
        try:
            statistics = go(url)
        except:
            return render(self.request, "error.html", {"url": url})
        context = {"url": url, "statistics": statistics}
        return render(self.request, "results.html", context)
