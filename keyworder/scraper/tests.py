from django.test import Client, TestCase
from django.urls import reverse


class ScraperTests(TestCase):
    def setUp(self):
        self.client = Client()

    def test_no_scheme_url(self):
        url = "djangoproject.com"
        response = self.client.post(reverse("home"), {"url": url})
        self.assertEqual(response.status_code, 200)

    def test_correct_http_url(self):
        url = "http://djangoproject.com"
        response = self.client.post(reverse("home"), {"url": url})
        self.assertEqual(response.status_code, 200)

    def test_correct_https_url(self):
        url = "https://djangoproject.com"
        response = self.client.post(reverse("home"), {"url": url})
        self.assertEqual(response.status_code, 200)

    def test_wrong_url(self):
        url = "django>project)com"
        response = self.client.post(reverse("home"), {"url": url})
        self.assertEqual(response.status_code, 200)
        self.assertIs(response.context["form"].is_valid(), False)

    def test_non_existing_url(self):
        url = "down.example.org"
        response = self.client.post(reverse("home"), {"url": url})
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "URL not accessible.")
