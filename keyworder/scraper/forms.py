from django import forms


class URLForm(forms.Form):
    url = forms.URLField(label="Website address", max_length=2048)
