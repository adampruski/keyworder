from django.urls import path
from scraper.views import HomePageView


urlpatterns = [path("", HomePageView.as_view(), name="home")]
