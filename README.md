# Keyworder
App for scraping meta keywords from website and counting their occurrence. Code written in python 3. It should be able deal with weird characters, because of it's normalization features implemented in code.

## Installation
Go to project directory and downloads required packages:
```
pip install -r requirements.txt
```

## Usage
Simply run this command in keyworder path:
```
./manage.py runserver
```
URL: http://localhost:8000

## Todo
- Documentation for functions,
- Tests for scraper.py,
- Dockerfile and docker-compose for production use.
